import { NgModule } from '@angular/core';
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";

import { AppComponent } from './app.component';
import { CalculusComponent } from './calculus/calculus.component';

import { AppRoutingModule, ChildsComponents } from "./app-routing.module";

/*
Les fichiers modules représentent, comme leur nom l'indique, un
module applicatif angular.
Une application angular doit forcément avoir un NgModule principal qui
sera le point de départ de l'application et l'endroit où on déclarera
les components, les autres modules et les routeurs utilisé par l'application.
Une application pourra avoir des sous modules afin de séparer les parties logique
de l'appli et les rendre ainsi plus facilement réutilisable (on pourrait avoir
un module Authentification/gestion users, un module pour les produits etc.)
*/

// const appRoutes: Routes = [
//     { path: 'list', component: ListComponent },
//     { path: 'first', component: FirstComponent },
//     //{ path: '', redirectTo: 'list', pathMatch: 'full' },
//     //{ path: '**', component: FirstComponent }
// ];

@NgModule({
    //Ici on déclare les modules (~librairies) qu'on utilisera dans cette application
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutingModule //déclaration plus propre
        // RouterModule.forRoot(appRoutes)
    ],
    //Ici on déclare tous les components de l'application/du module
    declarations: [
        AppComponent,
        ChildsComponents,
        // FirstComponent,
        // ListComponent,
        CalculusComponent
    ],
    //Ici on indique le component principale qui sera chargé initialement par l'application
    bootstrap: [AppComponent]

})
export class AppModule {}