import { Component } from '@angular/core';


/**
 * Un component représente un morceau d'interface avec son template
 * et sa logique js. Ils sont fait pour être paramètrables et réutilisables.
 * Une application Angular doit avoir une component "principale", celui
 * duquel partira toute l'application, ici c'est celui ci : AppComponent
 * Ce component pourra en charger d'autres, et c'est lui qui sera
 * chargé en premier par angular
 */

@Component({
    //selector représente la balise qu'on devra utiliser pour afficher ce component
    selector: 'app-root',
    //template représente le html du component (comme le twig), il est préférable de l'externaliser via templateUrl
    templateUrl: './app.component.html'
})
export class AppComponent {

}