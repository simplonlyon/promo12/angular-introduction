import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculus',
  templateUrl: './calculus.component.html',
  styleUrls: ['./calculus.component.css']
})
export class CalculusComponent implements OnInit {
  a:number;
  b:number;
  operation:string;

  constructor() { }

  ngOnInit() {
  }

  result() {
    return eval(this.a+this.operation+this.b);
  }

}
