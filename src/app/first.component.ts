import { Component } from '@angular/core';


@Component({
    selector: 'app-first',
    templateUrl: './first.component.html'
})
export class FirstComponent {
    /**
     * Les propriétés publiques d'un component seront accessibles dans le
     * template de celui ci
     */
    property = 'coucou';
    /**
     * Les méthodes publiques d'un component seront déclenchable par des
     * event dans le template du component
     */
    changeProperty(){
        this.property = 'kekchose';
    }
}