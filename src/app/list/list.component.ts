import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  list:string[];
  newItem:string;

  constructor() { }

  ngOnInit() {
    this.list = ['ga', 'zo', 'bu'];
  }

  add():void {
    this.list.push(this.newItem);
    this.newItem = '';
  }

}
