import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FirstComponent } from "./first.component";
import { ListComponent } from "./list/list.component";
// import { AppComponent } from "./app.component";

// routes
const appRoutes: Routes = [
    { path: 'list', component: ListComponent },
    { path: 'first', component: FirstComponent },
    //{ path: '', redirectTo: 'list', pathMatch: 'full' },
    //{ path: '**', component: FirstComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
export const ChildsComponents = [ListComponent, FirstComponent];